import java.io.*;
import java.util.*;

class StringTokenizerDemo{

	public static void main(String [] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String stdInfo = br.readLine();

		StringTokenizer st = new StringTokenizer(stdInfo," ");
	
		String stdName = st.nextToken();
		char grade = st.nextToken().charAt(0);
		int  marks = Integer.parseInt(st.nextToken());
		float percentage = Float.parseFloat(st.nextToken());

		System.out.println(stdName);
		System.out.println(grade);
		System.out.println(marks);
		System.out.println(percentage);
	}
}