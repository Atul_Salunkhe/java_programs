import java.io.*;

class BufferedReaderDemo {
	
	public static void main(String [] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter StudentName");
		String stdName = br.readLine();

		System.out.println("Enter Division");
		char div = (char)br.read();
	
		br.skip(2);
		
		System.out.println("Enter marks");
		int marks = Integer.parseInt(br.readLine());
				

		System.out.println(stdName);
		System.out.println(div);
		System.out.println(marks);
	}
}