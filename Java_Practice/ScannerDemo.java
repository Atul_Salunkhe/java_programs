import java.util.Scanner;

class ScannerDemo {
	
	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter StudentName");
		String stdName = sc.nextLine();

		System.out.println("Enter Division");
		char div = sc.nextLine().charAt(0);		

		System.out.println("Enter marks");
		int marks = sc.nextInt();
				
		System.out.println(stdName);
		System.out.println(div);
		System.out.println(marks);
	}
}